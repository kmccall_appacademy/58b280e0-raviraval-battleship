require_relative 'board.rb'
require_relative 'player.rb'
# require 'byebug'
class BattleshipGame
  attr_reader :board, :player, :current_player

  def initialize(player = HumanPlayer.new("Batman"), board)
    @board = board
    @player = player
    @current_player = @player
  end

  def attack(pos)
    # debugger
    if @board[pos] == :s
      puts "Hit!"
    else
      puts "Miss!"
    end
    @board[pos] = :x
  end

  def count
    @board.count
  end


  def play
    puts "Welcome to Battleship!"
    until game_over?
      play_turn
    end
  end

  def play_turn
    puts "Where do you want to shoot?"
    puts "Enter your target as \"x, y\""
    puts "Values between 0 and 9 are valid"
    attack(@current_player.get_play)
  end

  def game_over?
    return true if @board.won?
    false
  end

  def display_status
    puts "The seas look like this:"
    @board.display
    puts
    puts "There are #{self.count} ships remaining."

  end

end
