class HumanPlayer

  def initialize(name)
    @name = name
  end

  def get_play
    user_guess = gets.chomp
    [user_guess[0], user_guess[3]]
  end
end
