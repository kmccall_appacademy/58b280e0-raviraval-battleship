class Board
  attr_accessor :grid

  def initialize(grid = Board::default_grid)
    @grid = grid
  end

  def self.default_grid
    grid = Array.new(0)
    times = 0
    until times == 10
      grid << [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil]
      times += 1
    end
    grid
  end

  def empty?(pos = nil)
    unless pos == nil
      return true if grid[pos[0]][pos[1]] == nil
      return false
    else
      @grid.each_index do |rowidx|
        @grid[rowidx].each do |spot|
          if spot != nil
            return false
          end
        end
      end
      true
    end
  end

  def full?
    @grid.each_index do |rowidx|
      @grid[rowidx].each do |spot|
        if spot == nil
          return false
        end
      end
    end
    true
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def []=(pos, val)
    row, col = pos
    grid[row][col] = val
  end


  def count
    ship_count = 0
    @grid.each_index do |rowidx|
      @grid[rowidx].each do |spot|
        if spot == :s
          ship_count += 1
        end
      end
    end
    ship_count
  end

  def place_random_ship
    if self.full?
      raise "board is full"
    else
      pos = get_random_empty
      @grid[pos[0]][pos[1]] = :s
    end
  end

  def get_random_empty
    empty_res = []
    @grid.each_index do |rowidx|
      @grid[rowidx].each_index do |spotidx|
        if grid[rowidx][spotidx] == nil
          empty_res << [rowidx, spotidx]
        end
      end
    end
    empty_res.sample
  end

  def won?
    @grid.each_index do |rowidx|
      @grid[rowidx].each do |spot|
        if spot == :s
          return false
        end
      end
    end
    true
  end

  def display
    print @grid
  end

end
